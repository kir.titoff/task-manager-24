package ru.t1.ktitov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.ICommandRepository;
import ru.t1.ktitov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService extends ICommandRepository {

    void add(@Nullable AbstractCommand command);

    @NotNull
    AbstractCommand getCommandByArgument(@Nullable String argument);

    @NotNull
    AbstractCommand getCommandByName(@Nullable String name);

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

}
