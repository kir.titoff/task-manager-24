package ru.t1.ktitov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.IUserOwnedRepository;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

}
